module.exports = {
    env: {
      node: true,
    },
    extends: [
      'eslint:recommended',
      'plugin:vue/vue3-recommended',
    ],
    rules: {
      'vue/multi-word-component-names' : 'off',
      // override/add rules settings here, such as:
      'no-unused-vars': 'off',
      // 'vue/no-unused-vars': 'off',
      //  "vue/no-reserved-component-names": "off",
    }
  }